import { precacheAndRoute } from 'workbox-precaching'

precacheAndRoute(self.__WB_MANIFEST)

precacheAndRoute([
  { url: '/', revision: '2' },
  { url: '/favicon.ico', revision: '2' },
  { url: '/manifest.json', revision: '2' },
  { url: '/icon/192.png', revision: '2' }
])
