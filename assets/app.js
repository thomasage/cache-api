import './styles/app.css'

// start the Stimulus application
import './bootstrap'

const debug = document.getElementById('debug')
const output = document.getElementById('output')
const request = new Request('/api/tasks')

if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/service-worker.js')
      .then(registration => {
        debug.innerHTML = 'SW registered\n' + debug.innerHTML
        console.log('SW registered: ', registration)
      })
      .catch(registrationError => {
        debug.innerHTML = 'SW failed\n' + debug.innerHTML
        console.log('SW registration failed: ', registrationError)
      })
  })
}

doSomething()
  .then(() => {
    debug.innerHTML = 'doSomething DONE\n' + debug.innerHTML
  })

async function doSomething () {
  await fetchFromCache()
  await fetchFromNetwork()
}

async function fetchFromNetwork () {
  debug.innerHTML = 'FROM NETWORK\n' + debug.innerHTML
  try {
    const cache = await caches.open('cache-api')
    await cache.add(request)
    debug.innerHTML = 'NETWORK OK\n' + debug.innerHTML
    await fetchFromCache()
  } catch (error) {
    debug.innerHTML = 'NETWORK KO\n' + debug.innerHTML
  }
}

async function fetchFromCache () {
  debug.innerHTML = 'FROM CACHE\n' + debug.innerHTML
  try {
    const cache = await caches.open('cache-api')
    const response = await cache.match(request)
    const payload = await response.json()
    output.innerHTML = JSON.stringify(payload['hydra:member'], null, 4) + '\n'
    debug.innerHTML = 'CACHE OK\n' + debug.innerHTML
  } catch (error) {
    debug.innerHTML = 'CACHE KO\n' + debug.innerHTML
  }
}
